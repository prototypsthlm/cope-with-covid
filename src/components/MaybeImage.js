import React from 'react'

export default (props) => {
  const image = props.image
  try {
    if (image) {
      return (
        <div className={props.className}>
          <img src={require('../assets/' + props.image)} className="category" alt="Mood" />
        </div>
      )
    }
  } catch (err) {
    console.log(err)
  }
  return null
}
