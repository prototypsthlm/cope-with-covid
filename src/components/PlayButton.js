import React from 'react'
import { Button, Icon } from 'semantic-ui-react'

export default ({ onClick, style }) => (
  <Button
    onClick={onClick}
    circular
    icon
    primary
    size="massive"
    style={{ ...style, backgroundColor: '#f65c78' }}
  >
    <Icon name="play" />
  </Button>
)
