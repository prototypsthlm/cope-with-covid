import React from 'react'
import { Icon } from 'semantic-ui-react'
import { useHistory } from 'react-router-dom'
import { colors } from '../../colors'

export default function AppHeader({ title, hideGoBack = false }) {
  let history = useHistory()

  return (
    <div style={styles.container}>
      {!hideGoBack && (
        <Icon
          name="arrow left"
          onClick={() => history.goBack()}
          style={styles.arrow}
          size="large"
        />
      )}
      <h3 style={styles.header}>{title || 'Cope with Covid'}</h3>
    </div>
  )
}

const styles = {
  container: {
    width: '100%',
    height: '60px',
    backgroundColor: colors.yellow,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '8px 16px',
    position: 'sticky',
    top: '0',
    zIndex: '1',
  },
  arrow: {
    position: 'absolute',
    left: '16px',
    color: colors.black,
  },
  header: {
    margin: 0,
    color: colors.black,
  },
}
