import { NavLink } from 'react-router-dom';
import React from 'react';
import styles from './AppNavigation.module.css';
import { Icon } from 'semantic-ui-react';

export default function AppNavigation() {
  return (
    <div className={styles.container}>
      <div className={styles.navbar}>
          <NavLink exact className={styles.item} activeClassName='active-item' to="/home">
            <Icon name='home' size='big' className='color-white'/>
          </NavLink>
          <NavLink exact className={styles.item} activeClassName='active-item' to="/library">
            <Icon name='tasks' size='big' className='color-white'/>
          </NavLink>
          <NavLink exact className={styles.item} activeClassName='active-item' to="/mypage">
            <Icon name='heart' size='big' className='color-white'/>
          </NavLink>
      </div>
    </div>
  );
}

