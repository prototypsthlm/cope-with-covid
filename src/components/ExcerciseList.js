import { Button, Card, Icon } from 'semantic-ui-react'
import React from 'react'
import styles from './ExcerciseList.module.css'
import { useHistory, useParams } from 'react-router-dom';

export default function MyPage(props) {
  let { categoryId } = useParams()
  if (!categoryId) categoryId = "c1"; // TODO: Hack
  let history = useHistory();

  function start(id, toInstructions)  {
    let basePath = `/categories/${categoryId}/items/${id}`;
    let path = basePath + (toInstructions ? "/exercise" : "");
    history.push(path);
  }
  function evaluate (id)  {
    let path = `/categories/${categoryId}/items/${id}/evaluate`;
    history.push(path);
  }
  
  return (
    <Card.Group centered={true} style={{ marginTop: '20px' }}>
      {props.exercises.map((exercise) => {
        const iconName = exercise.type === 'sound-exercise' ? 'sound' : 'pencil alternate'
        return (
          <Card className={styles.card} fluid>
            <Card.Content style={{ backgroundColor: '#ffd271', paddingBottom: '7px' }}>
              <Icon name={iconName} style={{ float: 'right', fontSize: '1.2em' }} />
              <Card.Header>{exercise.name}</Card.Header>
            </Card.Content>
            <Card.Content>
              <Card.Description style={{ backgroundColor: 'white' }}>
                {exercise.description}
              </Card.Description>
            </Card.Content>
            <div style={{ display: 'flex', justifyContent: 'flex-end', padding: '8px' }}>
              {props.evaluate && <Button style={button} onClick={() => evaluate(exercise.id, 'c1')}>Utvärdera</Button> }
              <Button as="a" onClick={() => start(exercise.id, props.evaluate)} style={button}>
                Starta
              </Button>
            </div>
          </Card>
        )
      })}
    </Card.Group>
  )
}

const button = {
  backgroundColor: '#f65c78',
  marginTop: '0',
  color: 'white',
  font: 'Nunito',
}
