import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import StartPage from './pages/StartPage'
import LibraryPage from './pages/LibraryPage'
import MyPage from './pages/MyPage'
import CategoryPage from './pages/CategoryPage'
import ItemPage from './pages/ItemPage'
import ExercisePage from './pages/ExercisePage'
import EvaluatePage from './pages/EvaluatePage'
import React from 'react'
import WelcomePage from './pages/WelcomePage'
import AppNavigation from './components/navigation/AppNavigation'

export default function AppRouter() {
  // eslint-disable-next-line no-unused-vars
  const hasSeenWelcome = document.cookie.includes('seen-welcome')

  return (
    <Router basename={process.env.PUBLIC_URL}>
        <>
          <Switch>
            <Route exact path="/">
              <WelcomePage/>
            </Route>
            <Route exact path="/home">
              <StartPage title={'Dagens cope!'}/>
              <AppNavigation />
            </Route>
            <Route path="/library">
              <LibraryPage />
              <AppNavigation />
            </Route>
            <Route path="/mypage">
              <MyPage />
              <AppNavigation />
            </Route>
            <Route path="/categories/:categoryId/items/:id/evaluate">
              <EvaluatePage />
              <AppNavigation />
            </Route>
            <Route path="/categories/:categoryId/items/:id/exercise">
              <ExercisePage />
              <AppNavigation />
            </Route>
            <Route path="/categories/:categoryId/items/:id">
              <ItemPage />
              <AppNavigation />
            </Route>
            <Route path="/categories/:id">
              <CategoryPage />
              <AppNavigation />
            </Route>
          </Switch>
        </>
    </Router>
  )
}
