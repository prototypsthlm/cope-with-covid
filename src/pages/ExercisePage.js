import React from 'react'
import { useParams } from 'react-router-dom'
import findById from '../components/jsonPath'
import AppHeader from '../components/navigation/AppHeader'
import PlayButton from '../components/PlayButton'
import styles from './ExercisePage.module.css'

const Instruction = (props) => {
  const data = props.data
  switch (data.type) {
    case 'text':
      return <p>{data.description}</p>
    case 'icon':
      return (
        <div className={styles.iconContainer}>
          <img src={require('../assets/' + data.icon)} className={styles.icon} alt="Mood" />
          {data.description}
        </div>
      ) // TODO: class name
    case 'heading-and-list':
      return (
        <>
          <p>{data.heading}</p>
          <ul>
            {data.items.map((i) => (
              <li>{i}</li>
            ))}
          </ul>
        </>
      )
    case 'sound':
      return (
        <div className={styles.soundContainer}>
          <PlayButton />
        </div>
      )
    default:
      return null
  }
}

export default function ExercisePage() {
  const { id } = useParams()
  const item = findById(id)

  return (
    <>
      <AppHeader title={item.name} />
      <div className={styles.container}>
        <div>
          {item.instructions.map((instruction) => (
            <Instruction data={instruction} />
          ))}
        </div>
      </div>
    </>
  )
}
