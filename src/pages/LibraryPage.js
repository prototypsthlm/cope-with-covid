import React from 'react'
import { Link } from 'react-router-dom'
import MaybeImage from '../components/MaybeImage'
import styles from './LibraryPage.module.css'
import data from '../data/data.json'
import AppHeader from '../components/navigation/AppHeader'

const categories = data.categories

export default function LibraryPage() {
  return (
    <>
      <AppHeader title="Vad behöver du hjälp med?" hideGoBack />
      <div className="library">
        <ul className={styles.categoryList}>
          {categories.map((c) => (
            <Link className={styles.categoryListItem} key={c.id} to={`/categories/${c.id}`}>
              <MaybeImage className={styles.imgContainer} image={c.image} />
              <p>{c.name}</p>
            </Link>
          ))}
        </ul>
      </div>
    </>
  )
}
