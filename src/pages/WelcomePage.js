import React from 'react'
import { Container } from 'semantic-ui-react'
import { useHistory } from 'react-router-dom'
import PlayButton from '../components/PlayButton'

export default function WelcomePage() {
  let history = useHistory()

  const getStarted = () => {
    const date = new Date()
    date.setYear(2060)
    document.cookie = `seen-welcome=; expires=${date};`
    history.push('/home')
  }
  return (
    <div className="welcome-container">
      <div className="orange-blob" />

      <Container text textAlign="center">
        <h1>Cope with Covid</h1>
        <p>
          <strong>
            Känner du dig stressad,
            <br />
            ensam eller orolig?
          </strong>
        </p>
        <p>
          Dessa reaktioner är helt naturliga under en kris. Covid-19 har på väldigt kort tid
          förändrat mångas livssituation vilket kan vara svårt.
        </p>
        <p>
          <em>Cope with Covid</em> lär dig hur du bättre kan hantera dina egna känslomässiga
          reaktioner i samband med Covid-19-pandemin.
        </p>
        <p>
          Den här appen bygger på evidensbaserade metoder hämtade från kognitiv beteende terapi och
          accepance and commitment therapy.
        </p>
        <PlayButton onClick={getStarted} style={{ marginTop: '30px' }} />
      </Container>
      <div className="green-blob" />
    </div>
  )
}
