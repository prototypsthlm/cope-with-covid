import React from 'react'
import { Container } from 'semantic-ui-react'
import data from '../data/data.json'
import ExcerciseList from '../components/ExcerciseList'
import AppHeader from '../components/navigation/AppHeader'

const exercises = data.categories
  .map((category) => category.items.filter((item) => item.type.indexOf('exercise') !== -1))
  .flat()

console.log('In: , exercises: ', exercises)

export default function MyPage() {
  return (
    <>
      <AppHeader title="Sparade verktyg" hideGoBack />

      <Container style={{ marginBottom: "100px" }}>
        <ExcerciseList exercises={exercises} evaluate="true" />
      </Container>
    </>
  )
}
