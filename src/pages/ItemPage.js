import { Icon, Container, Card } from 'semantic-ui-react'
import React from 'react'
import { useHistory, useParams } from 'react-router-dom'

import findById from '../components/jsonPath'
import AppHeader from '../components/navigation/AppHeader'
import { colors } from '../colors'
import PlayButton from '../components/PlayButton'

export default function ItemPage() {
  let { categoryId, id } = useParams()
  let item = findById(id)
  // eslint-disable-next-line no-unused-vars
  let category = findById(categoryId)
  let history = useHistory();

  const startExercise = () => {
    // TODO: local-storage
    let path = `/categories/${categoryId}/items/${id}/exercise`;
    history.push(path);
  }

  return (
    <>
      <AppHeader title=" " />

      <Container>
        <Card fluid>
          <Card.Content style={{ backgroundColor: '#ffd271', paddingBottom: '7px' }}>
            <Card.Header style={{ fontFamily: 'Nunito' }}>{item.name}</Card.Header>
          </Card.Content>
          <Card.Content>
            <Card.Meta style={{ fontStyle: 'italic' }}>Varför</Card.Meta>
            <Card.Description style={{ backgroundColor: 'white', color: colors.gray }}>
              {item.why.map((paragraph) => (
                <div style={{ display: 'flex', justifyContent: 'left', marginBottom: '8px' }}>
                  <Icon name="plus" style={{ marginRight: '10px' }} />
                  <p>{paragraph}</p>
                </div>
              ))}
            </Card.Description>
            <Card.Meta style={{ marginTop: '20px', fontStyle: 'italic' }}>Hur</Card.Meta>
            <Card.Description style={{ backgroundColor: 'white' }}>
              {item.description.map((paragraph) => (
                <p>{paragraph}</p>
              ))}
            </Card.Description>
          </Card.Content>
          <Card.Content extra style={{ display: 'flex', justifyContent: 'center' }}>
            <PlayButton onClick={startExercise} />
          </Card.Content>
        </Card>
      </Container>
    </>
  )
}
