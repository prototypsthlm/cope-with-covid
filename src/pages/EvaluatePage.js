import React from 'react'
import { Container, Header, Card, Button } from 'semantic-ui-react'
import { useParams } from 'react-router-dom';

import findById from "../components/jsonPath"
import AppHeader from '../components/navigation/AppHeader'

import styles from '../components/ExcerciseList.module.css'

export default function EvaluatePage() {
  let { id } = useParams();
  let item = findById(id);

  return (
    <>
      <AppHeader title='Utvärdering'/>

      <Container>
        <Header as="h2" style={{ textAlign: "center" }}>{item.name}</Header>
        <form>
        <Card.Group centered={true} style={{ marginTop: '20px' }}>
          <Card className={styles.card} fluid>
            <Card.Content style={{ backgroundColor: '#ffd271', paddingBottom: '7px' }}>
              <Header as="h4">Hur hjälpsam var övningen för dig?</Header>
            </Card.Content>
            <Card.Content>
              <Card.Description className="radio-container" style={{ backgroundColor: 'white' }}>
                <label><input type="radio" name="eval" value="1" />1</label>
                <label><input type="radio" name="eval" value="2" />2</label>
                <label><input type="radio" name="eval" value="3" />3</label>
                <label><input type="radio" name="eval" value="4" />4</label>
                <label><input type="radio" name="eval" value="5" />5</label>
                </Card.Description>
            </Card.Content>
          </Card>
          <Card className={styles.card} fluid>
            <Card.Content style={{ backgroundColor: '#ffd271', paddingBottom: '7px' }}>
              <Header as="h4">Skriv ner dina egna reflektioner:</Header>
            </Card.Content>
            <Card.Content>
              <Card.Description style={{ backgroundColor: 'white' }}>
                <textarea style={{ width: "100%", height: "100px"}}></textarea>
                </Card.Description>
            </Card.Content>
          </Card>
          </Card.Group>
          <div style={{ display: 'flex', justifyContent: 'flex-end', padding: '8px' }}>
            <Button style={button}>Spara</Button>
          </div>
        </form>
      </Container>
      <Container>
        <div style={{ marginTop: "20px", textAlign: "center" }}>
          <strong>Gillade du övningen?<br/>Kom ihåg att spara den i din verktygslåda!</strong>
        </div>
      </Container>
    </>
  )
}

const button = {
  backgroundColor: '#f65c78',
  marginTop: '0',
  color: 'white',
  font: 'Nunito',
}
