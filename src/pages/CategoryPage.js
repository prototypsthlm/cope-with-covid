import React from 'react'
import AppHeader from '../components/navigation/AppHeader'
import { useParams } from 'react-router-dom'
import findById from '../components/jsonPath'
import MaybeImage from '../components/MaybeImage'
import { Container } from 'semantic-ui-react'
import styles from './CategoryPage.module.css'
import ExcerciseList from '../components/ExcerciseList'

export default function CategoryPage() {
  let { id } = useParams()
  let category = findById(id)

  return (
    <>
      <AppHeader title={category.name} />
      <Container className={styles.container}>
        <MaybeImage className={styles.img} image={category.image} />
        <p className={styles.text}>{category.description}</p>
        <ExcerciseList exercises={category.items} />
      </Container>
    </>
  )
}
