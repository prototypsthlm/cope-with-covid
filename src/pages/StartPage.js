import React, { useState } from 'react'
import { Button, Container, Image } from 'semantic-ui-react'
import walk from '../assets/walk.svg'
import yoga from '../assets/yoga.svg'
import smartphone from '../assets/smartphone.svg'
import news from '../assets/news.svg'
import AppHeader from '../components/navigation/AppHeader'

const exampleCopeData = [
  {
    title: 'Ta en promenad',
    text: 'När du rör på dig frigörs hormoner och signalsubstanser som får dig att må bättre.',
    date: 'Dag 1',
    image: walk,
  },
  {
    title: 'Ring en vän',
    text: 'Att ta kontakta dina vänner och bekanta bidrar till ökat psykiskt välmående.',
    date: 'Dag 2',
    image: smartphone,
  },
  {
    title: 'Läs nyheter max två gånger om dagen',
    text:
      'Du kommer inte att få mer kontroll över världsläget genom att läsa nyheterna ofta istället aktiverar det kroppens alarmsystem och du känner dig mer stressad.',
    date: 'Dag 3',
    image: news,
  },
  {
    title: 'Ladda ner en träningsapp för hemmaträning',
    text: 'Fysisk aktivitet är viktigt för att minska ångest och nedstämdhet.',
    date: 'Dag 4',
    image: yoga,
  },
]

export default function StartPage() {
  const [copeIndex, setCope] = useState(0)

  const nextTip = () => {
    if (copeIndex === exampleCopeData.length - 1) setCope(0)
    else {
      setCope(copeIndex + 1)
    }
  }

  const cope = exampleCopeData[copeIndex]

  return (
    <>
      <AppHeader title="Dagens cope!" hideGoBack />
      <Container style={{ paddingTop: '16px' }}>
        <div style={styles.card}>
          <Image style={styles.img} src={cope.image} />

          <div style={styles.content}>
            <h3>{cope.title}</h3>

            <p style={styles.date}>{cope.date}</p>
            <p style={styles.text}>{cope.text}</p>

            <div style={styles.buttonContainer}>
              <Button onClick={nextTip} style={styles.button}>
                Nytt tips
              </Button>
            </div>
          </div>
        </div>
      </Container>
    </>
  )
}

const styles = {
  card: {
    backgroundColor: '#ffd271',
    width: '100%',
  },
  img: {
    width: '100%',
    padding: '24px',
    maxHeight: '300px',
  },
  content: {
    backgroundColor: 'white',
    padding: '16px',
    border: '1px solid #e4e4e4',
    borderTop: '0',
  },
  date: {
    opacity: '50%',
    fontSize: '12px',
    marginTop: '-10px',
    marginBottom: '16px',
  },
  description: {
    fontSize: '16px',
  },
  label: {
    display: 'flex',
    alignItems: 'center',
    backgroundColor: '#4d4d4d',
    padding: '12px 8px',
  },
  labelText: {
    margin: '0',
    color: '#c3f584',
    fontSize: '14px',
    marginLeft: '4px',
  },
  icon: {
    color: '#c3f584',
    fontSize: '1.2em',
  },
  buttonContainer: {
    display: 'flex',
    width: '100%',
    justifyContent: 'flex-end',
  },
  button: {
    backgroundColor: '#c3f584',
    marginTop: '25px',
    color: '#4d4d4d',
  },
}
