const colors = {
  red: '#f65c78',
  orange: '#ffd271',
  yellow: '#fff3af',
  green: '#c3f584',
  gray: '#4d4d4d',
  white: '#fff',
  black: '#000'
}

export { colors }
